unit Vcl.Styles.Preview;

interface

Uses
  System.Classes, System.Generics.Collections, Winapi.Windows, Vcl.Themes, Vcl.Styles,
  Vcl.Forms, Vcl.Graphics, Vcl.Controls, Vcl.ExtCtrls, Vcl.Styles.Utils.Graphics;

type
  TPreviewType = (ptOriginal, ptTabs);

  TVisualStylePreview = class(TCustomControl)
    protected
      FStyleName       : string;
      FIcon            : HICON;
      FCaption         : TCaption;
      FRegion          : HRGN;
      FBitmap          : TBitmap;
      FPreviewType     : TPreviewType;
      FFormBorderSize  : TRect;
      FBkgColor        : TColor;
      FUnavailableText : string;
      FSelectedText    : string;
      FHotText         : string;
      FNormalText      : string;
      FDisabledText    : string;
      FPressedText     : string;
      FButtonText      : string;
      FFileMenuText    : string;
      FEditMenuText    : string;
      FViewMenuText    : string;
      FHelpMenuText    : string;


      function  GetFormBorderSize : TRect;
      function  GetMainMenuRect : TRect;
      function  GetTabsRect : TRect;
      function  GetCustomStyleServices : TCustomStyleServices;

      function  GetCaptionHeight : integer;
      function  GetLeftFormBorderWidth : integer;
      function  GetRightFormBorderWidth : integer;
      function  GetBottomFormBorderHeight : integer;
      function  RectVCenter(var aRect : TRect; aBounds : TRect): TRect;

      procedure DrawCaption;
      procedure DrawFormBorders;
      procedure DrawMainMenu;
      procedure DrawToolButtons;
      procedure DrawButtons;
      procedure DrawTabs;
      procedure DrawDefaultPanel;
      procedure DrawOriginalPreview;
      procedure DrawTabsPreview;

      procedure Paint; override;

      property StyleServices   : TCustomStyleServices read GetCustomStyleServices;

    public
      constructor Create(AControl: TComponent); override;
      destructor  Destroy; override;
      procedure   AfterConstruction; override;

      property Icon            : HICON                read FIcon            write FIcon;
      property StyleName       : string               read FStyleName       write FStyleName;
      property Caption         : TCaption             read FCaption         write FCaption;
      property Bitmap          : TBitmap              read FBitmap          write FBitmap;
      property UnavailableText : string               read FUnavailableText write FUnavailableText;
      property SelectedText    : string               read FSelectedText    write FSelectedText;
      property HotText         : string               read FHotText         write FHotText;
      property NormalText      : string               read FNormalText      write FNormalText;
      property DisabledText    : string               read FDisabledText    write FDisabledText;
      property PressedText     : string               read FPressedText     write FPressedText;
      property ButtonText      : string               read FButtonText      write FButtonText;
      property FileMenuText    : string               read FFileMenuText    write FFileMenuText;
      property EditMenuText    : string               read FEditMenuText    write FEditMenuText;
      property ViewMenuText    : string               read FViewMenuText    write FViewMenuText;
      property HelpMenuText    : string               read FHelpMenuText    write FHelpMenuText;

    published
      property PreviewType     : TPreviewType         read FPreviewType     write FPreviewType;
      property BkgColor        : TColor               read FBkgColor        write FBkgColor;
      property Align;
      property Anchors;
      property Visible;
  end;

implementation

uses
  System.SysUtils, System.Types, System.UITypes,
  Vcl.Styles.Utils.Misc;

const
  ORIGINAL_PPI = 96;

constructor TVisualStylePreview.Create(AControl: TComponent);
begin
  inherited Create(AControl);

  FRegion         := 0;
  FStyleName      := '';
  FCaption        := '';
  FIcon           := 0;
  FBitmap         := nil;
  FPreviewType    := ptOriginal;
  FFormBorderSize := rect(0, 0, 0, 0);
  FBkgColor       := clNone;

  FUnavailableText := 'Preview not available';
  FSelectedText    := 'Selected';
  FHotText         := 'Hot';
  FNormalText      := 'Normal';
  FDisabledText    := 'Disabled';
  FPressedText     := 'Pressed';
  FButtonText      := 'ToolButton';
  FFileMenuText    := 'File';
  FEditMenuText    := 'Edit';
  FViewMenuText    := 'View';
  FHelpMenuText    := 'Help';
end;

destructor TVisualStylePreview.Destroy;
begin
  try
    if (FRegion <> 0) then
      begin
        DeleteObject(FRegion);
        FRegion := 0;
      end;

    if (FBitmap <> nil) then FreeAndNil(FBitmap);
  finally
    inherited Destroy;
  end;
end;

procedure TVisualStylePreview.AfterConstruction;
begin
  inherited AfterConstruction;

  FBitmap             := TBitmap.Create;
  FBitmap.PixelFormat := pf32bit;
end;

function TVisualStylePreview.GetCaptionHeight : integer;
var
  LSize     : TSize;
  LDetails  : TThemedElementDetails;
  LServices : TCustomStyleServices;
begin
  LServices := StyleServices;
  LDetails  := LServices.GetElementDetails(twCaptionActive);

  if Assigned(Application.Mainform) then
    LServices.GetElementSize(0, LDetails, esActual, LSize, Application.MainForm.Monitor.PixelsPerInch)
   else
    LServices.GetElementSize(0, LDetails, esActual, LSize, Screen.PixelsPerInch);

  Result := LSize.cy;
end;

function TVisualStylePreview.GetLeftFormBorderWidth : integer;
var
  LSize     : TSize;
  LDetails  : TThemedElementDetails;
  LServices : TCustomStyleServices;
begin
  LServices := StyleServices;
  LDetails  := LServices.GetElementDetails(twFrameLeftActive);

  if Assigned(Application.Mainform) then
    LServices.GetElementSize(0, LDetails, esActual, LSize, Application.MainForm.Monitor.PixelsPerInch)
   else
    LServices.GetElementSize(0, LDetails, esActual, LSize, Screen.PixelsPerInch);

  Result := LSize.cx;
end;

function TVisualStylePreview.GetRightFormBorderWidth : integer;
var
  LSize     : TSize;
  LDetails  : TThemedElementDetails;
  LServices : TCustomStyleServices;
begin
  LServices := StyleServices;
  LDetails  := LServices.GetElementDetails(twFrameRightActive);

  if Assigned(Application.Mainform) then
    LServices.GetElementSize(0, LDetails, esActual, LSize, Application.MainForm.Monitor.PixelsPerInch)
   else
    LServices.GetElementSize(0, LDetails, esActual, LSize, Screen.PixelsPerInch);

  Result := LSize.cx;
end;

function TVisualStylePreview.GetBottomFormBorderHeight : integer;
var
  LSize     : TSize;
  LDetails  : TThemedElementDetails;
  LServices : TCustomStyleServices;
begin
  LServices := StyleServices;
  LDetails  := LServices.GetElementDetails(twFrameBottomActive);

  if Assigned(Application.Mainform) then
    LServices.GetElementSize(0, LDetails, esActual, LSize, Application.MainForm.Monitor.PixelsPerInch)
   else
    LServices.GetElementSize(0, LDetails, esActual, LSize, Screen.PixelsPerInch);

  Result := LSize.cy;
end;

function TVisualStylePreview.GetFormBorderSize: TRect;
begin
  Result.Top    := GetCaptionHeight;
  Result.Left   := GetLeftFormBorderWidth;
  Result.Right  := GetRightFormBorderWidth;
  Result.Bottom := GetBottomFormBorderHeight;
end;

function TVisualStylePreview.GetMainMenuRect : TRect;
const
  MENU_ITEM_HEIGHT = 20;
begin
  Result.Left   := FFormBorderSize.Left;
  Result.Top    := FFormBorderSize.Top;
  Result.Right  := FBitmap.Width - FFormBorderSize.Right;

  if Assigned(Application.Mainform) then
    Result.Bottom := Result.Top + MulDiv(MENU_ITEM_HEIGHT, Application.MainForm.Monitor.PixelsPerInch, ORIGINAL_PPI)
   else
    Result.Bottom := Result.Top + MulDiv(MENU_ITEM_HEIGHT, Screen.PixelsPerInch, ORIGINAL_PPI);
end;

function TVisualStylePreview.GetTabsRect : TRect;
const
  TABS_HEIGHT = 27;
begin
  Result.Left   := FFormBorderSize.Left;
  Result.Top    := FFormBorderSize.Top;
  Result.Right  := FBitmap.Width - FFormBorderSize.Right;

  if Assigned(Application.Mainform) then
    Result.Bottom := Result.Top + MulDiv(TABS_HEIGHT, Application.MainForm.Monitor.PixelsPerInch, ORIGINAL_PPI)
   else
    Result.Bottom := Result.Top + MulDiv(TABS_HEIGHT, Screen.PixelsPerInch, ORIGINAL_PPI);
end;

function TVisualStylePreview.GetCustomStyleServices : TCustomStyleServices;
begin
  Result := TStyleManager.Style[FStyleName];
end;

function TVisualStylePreview.RectVCenter(var aRect : TRect; aBounds : TRect): TRect;
begin
  OffsetRect(aRect, - aRect.Left, - aRect.Top);
  OffsetRect(aRect, 0, (aBounds.Height - aRect.Height) div 2);
  OffsetRect(aRect, aBounds.Left, aBounds.Top);

  Result := aRect;
end;

procedure TVisualStylePreview.DrawDefaultPanel;
var
  LDetails  : TThemedElementDetails;
  LColor    : TColor;
  LRect     : TRect;
  LServices : TCustomStyleServices;
begin
  LServices := StyleServices;
  LRect     := rect(0, 0, FBitmap.Width, FBitmap.Height);

  if (csDesigning in ComponentState) then
    begin
      if (FBkgColor <> clNone) then
        FBitmap.Canvas.Brush.Color := FBkgColor
       else
        FBitmap.Canvas.Brush.Color := clWhite;

      FBitmap.Canvas.Brush.Style := bsSolid;
      FBitmap.Canvas.FillRect(LRect);
      exit;
    end;

  if (FBkgColor <> clNone) then
    LColor := FBkgColor
   else
    if assigned(LServices) then
      begin
        LDetails := LServices.GetElementDetails(tpPanelBackground);

        if not(LServices.GetElementColor(LDetails, ecFillColor, LColor)) then
          LColor := GetSysColor(COLOR_BTNFACE);
      end;

  FBitmap.Canvas.Brush.Color := LColor;
  FBitmap.Canvas.Brush.Style := bsSolid;
  FBitmap.Canvas.FillRect(LRect);

  if (length(FUnavailableText) > 0) then
    begin
      if not(assigned(LServices)) or not(LServices.GetElementColor(LDetails, ecTextColor, LColor)) then
        LColor := GetSysColor(COLOR_BTNTEXT);

      FBitmap.Canvas.Font.Color := LColor;
      FBitmap.Canvas.TextRect(LRect, FUnavailableText, [tfVerticalCenter, tfCenter, tfSingleLine]);
    end;
end;

procedure TVisualStylePreview.DrawCaption;
var
  LClientRect     : TRect;
  LCaptionRect    : TRect;
  LTextRect       : TRect;
  LIconRect       : TRect;
  LButtonRect     : TRect;
  LScaledBtnRect  : TRect;
  LDetails        : TThemedElementDetails;
  LCaptionDetails : TThemedElementDetails;
  LIconDetails    : TThemedElementDetails;
  LRegion         : HRGN;
  LPPI            : integer;
  LPrevLeft       : integer;
  LServices       : TCustomStyleServices;
begin
  LServices    := StyleServices;
  LClientRect  := ClientRect;
  LCaptionRect := Rect(0, 0, FBitmap.Width, FFormBorderSize.Top);

  if Assigned(Application.Mainform) then
    LPPI := Application.MainForm.Monitor.PixelsPerInch
   else
    LPPI := Screen.PixelsPerInch;

  //Draw background
  LDetails.Element := teWindow;
  LDetails.Part    := 0;
  DrawStyleElement(FBitmap.Canvas.Handle, LDetails, LClientRect, True, LServices);

  //Draw caption border
  LDetails := LServices.GetElementDetails(twCaptionActive);

  LRegion := FRegion;
  try
    LServices.GetElementRegion(LDetails, LClientRect, FRegion);
    SetWindowRgn(Handle, FRegion, True);
  finally
    if (LRegion <> 0) then DeleteObject(LRegion);
  end;

  DrawStyleElement(FBitmap.Canvas.Handle, LDetails, LCaptionRect, True, LServices);
  LTextRect       := LCaptionRect;
  LCaptionDetails := LDetails;

  //Draw icon
  LIconDetails := LServices.GetElementDetails(twSysButtonNormal);
  LIconRect    := Rect(0, 0, GetSystemMetrics(SM_CXSMICON), GetSystemMetrics(SM_CYSMICON));

  if not(LServices.GetElementContentRect(0, LIconDetails, LCaptionRect, LButtonRect)) then
    LButtonRect := Rect(0, 0, 0, 0);

  RectVCenter(LIconRect, LButtonRect);

  if (LButtonRect.Width > 0) and (FIcon <> 0) then
    DrawIconEx(FBitmap.Canvas.Handle, LIconRect.Left, LIconRect.Top, FIcon, 0, 0, 0, 0, DI_NORMAL);

  Inc(LTextRect.Left, LButtonRect.Width + MulDiv(5, LPPI, ORIGINAL_PPI));

  //Draw buttons

  //Close button
  LDetails := LServices.GetElementDetails(twCloseButtonNormal);
  if LServices.GetElementContentRect(0, LDetails, LCaptionRect, LButtonRect) then
    begin
      if (LPPI > ORIGINAL_PPI) then
        begin
          LScaledBtnRect := LButtonRect;
          LScaledBtnRect.Left := LScaledBtnRect.Right - MulDiv(LButtonRect.Width, LPPI, ORIGINAL_PPI);

          if (LScaledBtnRect.Top > 1) then
            LScaledBtnRect.Top :=  MulDiv(LScaledBtnRect.Top, LPPI, Screen.DefaultPixelsPerInch);

          LScaledBtnRect.Bottom := LScaledBtnRect.Top + MulDiv(LButtonRect.Height, LPPI, ORIGINAL_PPI);
        end
       else
        LScaledBtnRect := LButtonRect;

      DrawStyleElement(FBitmap.Canvas.Handle, LDetails, LScaledBtnRect, True, LServices);
      LPrevLeft := LScaledBtnRect.Left;
    end
   else
    LPrevLeft := LCaptionRect.Right;

  //Maximize button
  LDetails := LServices.GetElementDetails(twMaxButtonNormal);
  if LServices.GetElementContentRect(0, LDetails, LCaptionRect, LButtonRect) then
    begin
      if (LPPI > ORIGINAL_PPI) then
        begin
          LScaledBtnRect := LButtonRect;

          if (LScaledBtnRect.Right > LPrevLeft) then
            LScaledBtnRect.Right := LPrevLeft;

          LScaledBtnRect.Left := LScaledBtnRect.Right - MulDiv(LButtonRect.Width, LPPI, ORIGINAL_PPI);

          if (LScaledBtnRect.Top > 1) then
            LScaledBtnRect.Top :=  MulDiv(LScaledBtnRect.Top, LPPI, Screen.DefaultPixelsPerInch);

          LScaledBtnRect.Bottom := LScaledBtnRect.Top + MulDiv(LButtonRect.Height, LPPI, ORIGINAL_PPI);
        end
       else
        LScaledBtnRect := LButtonRect;

      DrawStyleElement(FBitmap.Canvas.Handle, LDetails, LScaledBtnRect, True, LServices);
      LPrevLeft := LScaledBtnRect.Left;
    end;

  //Minimize button
  LDetails := LServices.GetElementDetails(twMinButtonNormal);
  if LServices.GetElementContentRect(0, LDetails, LCaptionRect, LButtonRect) then
    begin
      if (LPPI > ORIGINAL_PPI) then
        begin
          LScaledBtnRect := LButtonRect;

          if (LScaledBtnRect.Right > LPrevLeft) then
            LScaledBtnRect.Right := LPrevLeft;

          LScaledBtnRect.Left := LScaledBtnRect.Right - MulDiv(LButtonRect.Width, LPPI, ORIGINAL_PPI);

          if (LScaledBtnRect.Top > 1) then
            LScaledBtnRect.Top :=  MulDiv(LScaledBtnRect.Top, LPPI, Screen.DefaultPixelsPerInch);

          LScaledBtnRect.Bottom := LScaledBtnRect.Top + MulDiv(LButtonRect.Height, LPPI, ORIGINAL_PPI);
        end
       else
        LScaledBtnRect := LButtonRect;

      DrawStyleElement(FBitmap.Canvas.Handle, LDetails, LScaledBtnRect, True, LServices);
      LPrevLeft := LScaledBtnRect.Left;
    end;

  //Help button
  LDetails := LServices.GetElementDetails(twHelpButtonNormal);
  if LServices.GetElementContentRect(0, LDetails, LCaptionRect, LButtonRect) then
    begin
      if (LPPI > ORIGINAL_PPI) then
        begin
          LScaledBtnRect := LButtonRect;

          if (LScaledBtnRect.Right > LPrevLeft) then
            LScaledBtnRect.Right := LPrevLeft;

          LScaledBtnRect.Left := LScaledBtnRect.Right - MulDiv(LButtonRect.Width, LPPI, ORIGINAL_PPI);

          if (LScaledBtnRect.Top > 1) then
            LScaledBtnRect.Top :=  MulDiv(LScaledBtnRect.Top, LPPI, Screen.DefaultPixelsPerInch);

          LScaledBtnRect.Bottom := LScaledBtnRect.Top + MulDiv(LButtonRect.Height, LPPI, ORIGINAL_PPI);
        end
       else
        LScaledBtnRect := LButtonRect;

      DrawStyleElement(FBitmap.Canvas.Handle, LDetails, LScaledBtnRect, True, LServices);
    end;

  if (LButtonRect.Left > 0) then LTextRect.Right := LButtonRect.Left;

  //Draw text
  LServices.DrawText(FBitmap.Canvas.Handle, LCaptionDetails, FCaption, LTextRect, [tfLeft, tfSingleLine, tfVerticalCenter], clNone, LPPI);
end;

procedure TVisualStylePreview.DrawFormBorders;
var
  LRect     : TRect;
  LDetails  : TThemedElementDetails;
  LServices : TCustomStyleServices;
begin
  LServices := StyleServices;

  //Draw left border
  LRect    := Rect(0, FFormBorderSize.Top, FFormBorderSize.Left, FBitmap.Height - FFormBorderSize.Bottom);
  LDetails := LServices.GetElementDetails(twFrameLeftActive);
  DrawStyleElement(FBitmap.Canvas.Handle, LDetails, LRect, True, LServices);

  //Draw right border
  LRect    := Rect(FBitmap.Width - FFormBorderSize.Right, FFormBorderSize.Top, FBitmap.Width, FBitmap.Height - FFormBorderSize.Bottom);
  LDetails := LServices.GetElementDetails(twFrameRightActive);
  DrawStyleElement(FBitmap.Canvas.Handle, LDetails, LRect, True, LServices);

  //Draw Bottom border
  LRect    := Rect(0, FBitmap.Height - FFormBorderSize.Bottom, FBitmap.Width, FBitmap.Height);
  LDetails := LServices.GetElementDetails(twFrameBottomActive);
  DrawStyleElement(FBitmap.Canvas.Handle, LDetails, LRect, True, LServices);
end;

procedure TVisualStylePreview.DrawMainMenu;
const
  MENU_ITEM_WIDTH = 30;
var
  LMenuRect : TRect;
  LItemRect : TRect;
  LDetails  : TThemedElementDetails;
  LColor    : TColor;
  LWidth    : integer;
  LServices : TCustomStyleServices;
begin
  LServices := StyleServices;
  LMenuRect := GetMainMenuRect;

  LDetails := LServices.GetElementDetails(tmMenuBarBackgroundActive);
  DrawStyleElement(FBitmap.Canvas.Handle, LDetails, LMenuRect, True, LServices);

  LDetails := LServices.GetElementDetails(tmMenuBarItemNormal);
  LServices.GetElementColor(LDetails, ecTextColor, LColor);

  if Assigned(Application.Mainform) then
    begin
      LWidth := MulDiv(MENU_ITEM_WIDTH, Application.MainForm.Monitor.PixelsPerInch, ORIGINAL_PPI);
      LItemRect.Left   := LMenuRect.Left + MulDiv(10, Application.MainForm.Monitor.PixelsPerInch, ORIGINAL_PPI);
      LItemRect.Top    := LMenuRect.Top  + MulDiv(3, Application.MainForm.Monitor.PixelsPerInch, ORIGINAL_PPI);
    end
   else
    begin
      LWidth := MulDiv(MENU_ITEM_WIDTH, Screen.PixelsPerInch, ORIGINAL_PPI);
      LItemRect.Left   := LMenuRect.Left + MulDiv(10, Screen.PixelsPerInch, ORIGINAL_PPI);
      LItemRect.Top    := LMenuRect.Top  + MulDiv(3, Screen.PixelsPerInch, ORIGINAL_PPI);
    end;

  LItemRect.Right  := LItemRect.Left + LWidth;
  LItemRect.Bottom := LMenuRect.Bottom;
  LServices.DrawText(FBitmap.Canvas.Handle, LDetails, FFileMenuText, LItemRect, [tfLeft], LColor);

  LItemRect.Left   := LItemRect.Right;
  LItemRect.Right  := LItemRect.Left + LWidth;
  LServices.DrawText(FBitmap.Canvas.Handle, LDetails, FEditMenuText, LItemRect, [tfLeft], LColor);

  LItemRect.Left   := LItemRect.Right;
  LItemRect.Right  := LItemRect.Left + LWidth;
  LServices.DrawText(FBitmap.Canvas.Handle, LDetails, FViewMenuText, LItemRect, [tfLeft], LColor);

  LItemRect.Left   := LItemRect.Right;
  LItemRect.Right  := LItemRect.Left + LWidth;
  LServices.DrawText(FBitmap.Canvas.Handle, LDetails, FHelpMenuText, LItemRect, [tfLeft], LColor);
end;

procedure TVisualStylePreview.DrawToolButtons;
const
  BUTTON_WIDTH  = 75;
  BUTTON_HEIGHT = 25;
  PANEL_PADDING = 10;
var
  LMenuRect   : TRect;
  LButtonRect : TRect;
  LDetails    : TThemedElementDetails;
  LColor      : TColor;
  i           : integer;
  LWidth      : integer;
  LHeight     : integer;
  LPadding    : integer;
  LServices   : TCustomStyleServices;
begin
  LServices := StyleServices;
  LMenuRect := GetMainMenuRect;

  if Assigned(Application.Mainform) then
    begin
      LWidth    := MulDiv(BUTTON_WIDTH,  Application.MainForm.Monitor.PixelsPerInch, ORIGINAL_PPI);
      LHeight   := MulDiv(BUTTON_HEIGHT, Application.MainForm.Monitor.PixelsPerInch, ORIGINAL_PPI);
      LPadding  := MulDiv(PANEL_PADDING, Application.MainForm.Monitor.PixelsPerInch, ORIGINAL_PPI);
    end
   else
    begin
      LWidth    := MulDiv(BUTTON_WIDTH,  Screen.PixelsPerInch, ORIGINAL_PPI);
      LHeight   := MulDiv(BUTTON_HEIGHT, Screen.PixelsPerInch, ORIGINAL_PPI);
      LPadding  := MulDiv(PANEL_PADDING, Screen.PixelsPerInch, ORIGINAL_PPI);
    end;

  LButtonRect.Left   := FFormBorderSize.Left + LPadding;
  LButtonRect.Top    := LMenuRect.Bottom + LPadding;
  LButtonRect.Right  := LButtonRect.Left + LWidth;
  LButtonRect.Bottom := LButtonRect.Top  + LHeight;

  for i := 1 to 3 do
    begin
      LDetails := LServices.GetElementDetails(ttbButtonNormal);
      DrawStyleElement(FBitmap.Canvas.Handle, LDetails, LButtonRect, True, LServices);

      LServices.GetElementColor(LDetails, ecTextColor, LColor);
      LServices.DrawText(FBitmap.Canvas.Handle, LDetails, FButtonText + IntToStr(i), LButtonRect, TTextFormatFlags(DT_VCENTER or DT_CENTER), LColor);

      LButtonRect.Left  := LButtonRect.Right;
      LButtonRect.Right := LButtonRect.Left + LWidth;
    end;
end;

procedure TVisualStylePreview.DrawButtons;
const
  BUTTON_WIDTH  = 75;
  BUTTON_HEIGHT = 25;
  PANEL_PADDING = 10;
var
  LButtonRect : TRect;
  LDetails    : TThemedElementDetails;
  LColor      : TColor;
  i           : integer;
  LCaption    : string;
  LWidth      : integer;
  LHeight     : integer;
  LPadding    : integer;
  LServices   : TCustomStyleServices;
begin
  LServices := StyleServices;

  if Assigned(Application.Mainform) then
    begin
      LWidth    := MulDiv(BUTTON_WIDTH,  Application.MainForm.Monitor.PixelsPerInch, ORIGINAL_PPI);
      LHeight   := MulDiv(BUTTON_HEIGHT, Application.MainForm.Monitor.PixelsPerInch, ORIGINAL_PPI);
      LPadding  := MulDiv(PANEL_PADDING, Application.MainForm.Monitor.PixelsPerInch, ORIGINAL_PPI);
    end
   else
    begin
      LWidth    := MulDiv(BUTTON_WIDTH,  Screen.PixelsPerInch, ORIGINAL_PPI);
      LHeight   := MulDiv(BUTTON_HEIGHT, Screen.PixelsPerInch, ORIGINAL_PPI);
      LPadding  := MulDiv(PANEL_PADDING, Screen.PixelsPerInch, ORIGINAL_PPI);
    end;

  LButtonRect.Left   := FFormBorderSize.Left + LPadding;
  LButtonRect.Right  := LButtonRect.Left + LWidth;
  LButtonRect.Bottom := FBitmap.Height - FFormBorderSize.Bottom - LPadding;
  LButtonRect.Top    := LButtonRect.Bottom - LHeight;

  for i := 1 to 4 do
    begin
      case i of
        1 :
          begin
            LDetails := LServices.GetElementDetails(tbPushButtonNormal);
            LCaption := FNormalText;
          end;

        2 :
          begin
            LDetails := LServices.GetElementDetails(tbPushButtonHot);
            LCaption := FHotText;
          end;

        3 :
          begin
            LDetails := LServices.GetElementDetails(tbPushButtonPressed);
            LCaption := FPressedText;
          end;

        4 :
          begin
            LDetails := LServices.GetElementDetails(tbPushButtonDisabled);
            LCaption := FDisabledText;
          end;
      end;

      DrawStyleElement(FBitmap.Canvas.Handle, LDetails, LButtonRect, True, LServices);
      LServices.GetElementColor(LDetails, ecTextColor, LColor);
      LServices.DrawText(FBitmap.Canvas.Handle, LDetails, LCaption, LButtonRect, TTextFormatFlags(DT_VCENTER or DT_CENTER), LColor);

      LButtonRect.Left  := LButtonRect.Right + LPadding;
      LButtonRect.Right := LButtonRect.Left  + LWidth;
    end;
end;

procedure TVisualStylePreview.DrawTabs;
const
  TAB_WIDTH  = 80;
  TAB_OFFSET = 3;
var
  LDetails  : TThemedElementDetails;
  LTabsRect : TRect;
  LItemRect : TRect;
  LWidth    : integer;
  LColor    : TColor;
  LFlags    : TTextFormat;
  LOffset   : integer;
  LServices : TCustomStyleServices;
begin
  LServices := StyleServices;

  if Assigned(Application.Mainform) then
    begin
      LWidth    := MulDiv(TAB_WIDTH,  Application.MainForm.Monitor.PixelsPerInch, ORIGINAL_PPI);
      LOffset   := MulDiv(TAB_OFFSET, Application.MainForm.Monitor.PixelsPerInch, ORIGINAL_PPI);
    end
   else
    begin
      LWidth    := MulDiv(TAB_WIDTH,  Screen.PixelsPerInch, ORIGINAL_PPI);
      LOffset   := MulDiv(TAB_OFFSET, Screen.PixelsPerInch, ORIGINAL_PPI);
    end;

  LTabsRect := GetTabsRect;
  LFlags    := TTextFormatFlags(DT_VCENTER or DT_CENTER);
  LColor    := clBlack;

  // Tabs background
  LDetails := StyleServices.GetElementDetails(ttPane);
  DrawStyleElement(FBitmap.Canvas.Handle, LDetails, LTabsRect, True, LServices);


  // Selected tab
  LItemRect       := LTabsRect;
  LItemRect.Right := LItemRect.Left + LWidth;

  LDetails := StyleServices.GetElementDetails(ttTabItemSelected);
  DrawStyleElement(FBitmap.Canvas.Handle, LDetails, LItemRect, True, LServices);
  LServices.GetElementColor(LDetails, ecTextColor, LColor);
  LServices.DrawText(FBitmap.Canvas.Handle, LDetails, FSelectedText, LItemRect, LFlags, LColor);


  // Hot tab
  LItemRect.Left  := succ(LItemRect.Right);
  LItemRect.Right := LItemRect.Left + LWidth;
  LItemRect.Top   := LTabsRect.Top  + LOffset;  // unselected tabs are slightly shorter

  LDetails := StyleServices.GetElementDetails(ttTabItemHot);
  DrawStyleElement(FBitmap.Canvas.Handle, LDetails, LItemRect, True, LServices);
  LServices.GetElementColor(LDetails, ecTextColor, LColor);
  LServices.DrawText(FBitmap.Canvas.Handle, LDetails, FHotText, LItemRect, LFlags, LColor);


  // Normal tab
  LItemRect.Left  := succ(LItemRect.Right);
  LItemRect.Right := LItemRect.Left + LWidth;

  LDetails := StyleServices.GetElementDetails(ttTabItemNormal);
  DrawStyleElement(FBitmap.Canvas.Handle, LDetails, LItemRect, True, LServices);
  LServices.GetElementColor(LDetails, ecTextColor, LColor);
  LServices.DrawText(FBitmap.Canvas.Handle, LDetails, FNormalText, LItemRect, LFlags, LColor);
end;

procedure TVisualStylePreview.DrawOriginalPreview;
begin
  FFormBorderSize := GetFormBorderSize;

  DrawCaption;
  DrawFormBorders;
  DrawMainMenu;
  DrawToolButtons;
  DrawButtons;
end;

procedure TVisualStylePreview.DrawTabsPreview;
begin
  FFormBorderSize := GetFormBorderSize;

  DrawCaption;
  DrawFormBorders;
  DrawTabs;
  DrawButtons;
end;

procedure TVisualStylePreview.Paint;
begin
  FBitmap.SetSize(ClientRect.Width, ClientRect.Height);

  if (FStyleName = '') then
    DrawDefaultPanel
   else
    case FPreviewType of
      ptOriginal : DrawOriginalPreview;
      ptTabs     : DrawTabsPreview;
    end;

  Canvas.Draw(0, 0, FBitmap);
end;

end.
