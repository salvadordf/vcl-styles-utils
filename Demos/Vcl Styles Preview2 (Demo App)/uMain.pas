unit uMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls, Vcl.Styles.Ext,
  Vcl.ActnList, Vcl.PlatformDefaultStyleActnCtrls, Vcl.ActnMan, Vcl.ToolWin,
  System.Actions, Vcl.Styles.Preview;

type
  TFrmMain = class(TForm)
    ListView1: TListView;
    Label1: TLabel;
    Button1: TButton;
    ActionManager1: TActionManager;
    ActionApplyStyle: TAction;
    Panel1: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ActionApplyStyleUpdate(Sender: TObject);
    procedure ActionApplyStyleExecute(Sender: TObject);
    procedure ListView1SelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
  private
    FLoading : Boolean;
    FPreview : TVisualStylePreview;

    procedure FillVclStylesList;
  end;

var
  FrmMain: TFrmMain;

implementation
uses
  IOUtils,
  Vcl.Themes,
  Vcl.Styles;

{$R *.dfm}

procedure TFrmMain.FormCreate(Sender: TObject);
begin
  FLoading := False;

  FPreview             := TVisualStylePreview.Create(Self);
  FPreview.PreviewType := ptTabs;
  FPreview.Parent      := Panel1;
  FPreview.BoundsRect  := Panel1.ClientRect;

  FillVclStylesList;
end;

procedure TFrmMain.FormShow(Sender: TObject);
begin
  if (ListView1.Items.Count > 0) then
    ListView1.Selected := ListView1.Items.Item[0];
end;

procedure TFrmMain.ListView1SelectItem(Sender: TObject; Item: TListItem;
  Selected: Boolean);
begin
  if Selected and not(FLoading) then
    begin
      FPreview.Caption   := Item.SubItems[1];
      FPreview.StyleName := Item.SubItems[1];
      FPreview.Repaint;
    end;
end;

procedure TFrmMain.ActionApplyStyleExecute(Sender: TObject);
begin
  if assigned(ListView1.Selected) and (ListView1.Selected.Caption = 'Resource') then
    TStyleManager.SetStyle(ListView1.Selected.SubItems[1]);
end;

procedure TFrmMain.ActionApplyStyleUpdate(Sender: TObject);
begin
  TCustomAction(Sender).Enabled := assigned(ListView1.Selected) and (ListView1.Selected.Caption = 'Resource');
end;

procedure TFrmMain.FillVclStylesList;
Var
 StyleName : string;
 Item      : TListItem;
begin
  FLoading := True;

  for StyleName in TStyleManager.StyleNames do
    if not SameText(StyleName, 'Windows') then
      begin
        Item         := ListView1.Items.Add;
        Item.Caption := 'Resource';
        Item.SubItems.Add('');
        Item.SubItems.Add(StyleName);
      end;

   FLoading := False;
end;



end.
